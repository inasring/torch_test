make_video () {
    ffmpeg -y -framerate 10 -i $1/%05d.png  -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p output/$1.mp4
}



models="SGD Adam Adagrad NGD"
for model in $models
do
    python optim_test_xor_show_ticks.py $model
    make_video "save_fig/"$model
done

#make_video save_fig/SGD
#make_video save_fig/NGD
#make_video save_fig/Adam
#make_video save_fig/Adagrad
