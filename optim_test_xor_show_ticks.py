from pathlib import Path
import torch
from torch.autograd import Variable
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.contour import QuadContourSet,ContourSet
import time
import sklearn.datasets as skd
from ngdopt import NGD
import typer


class Linear1DModel(torch.nn.Module):
    def __init__(self,hidden_layout,
                inp_shape,out_shape,
                cuda=True):
        super(Linear1DModel,self).__init__()
        if cuda:
            dtype = torch.cuda.FloatTensor
        else:
            dtype = torch.FloatTensor
        self.hidden_layout = hidden_layout
        self.inp_shape   = inp_shape
        self.out_shape  = out_shape
        # self.linear_layers = []
        # hidden = hidden_layout[0]
        # self.linear_layers.append(torch.nn.Linear(inp_shape,hidden))
        # for i in range(1,len(hidden_layout)):
        #     self.linear_layers.append(torch.nn.Linear(hidden,hidden_layout[i]))
        #     hidden = hidden_layout[i]
        # self.linear_layers.append(torch.nn.Linear(hidden,out_shape))
        # print (self.linear_layers)
        # self.layer1 = torch.nn.Linear(2,10)
        # self.layer2 = torch.nn.Linear(10,1)
        self.layer1 = torch.nn.Linear(inp_shape,hidden_layout[0])
        self.layer2 = torch.nn.Linear(hidden_layout[0],hidden_layout[1])
        self.layer3 = torch.nn.Linear(hidden_layout[1],hidden_layout[2])
        self.layer4 = torch.nn.Linear(hidden_layout[2],out_shape)

    def forward(self,x):
        l = torch.tanh(self.layer1(x))
        l = torch.tanh(self.layer2(l))
        l = torch.tanh(self.layer3(l))
        l = torch.sigmoid(self.layer4(l))
        y_pred = l
        return y_pred

def change_lr(optim,lr):
    for g in optim.param_groups:
            g['lr'] = lr


def main(model_name: str):
    dtype = torch.cuda.FloatTensor
    #dtype = torch.FloatTensor
    torch.set_default_tensor_type(dtype)
    cudnn_version = torch.backends.cudnn.version()
    print("cudnn version:",cudnn_version)

    x = np.array([[0,0],
                [1,1],
                [1,0],
                [0,1]])
    y = np.array([[0],
                [0],
                [1],
                [1]])
    print(y.shape)
    print(x.shape)

    n_samples = 500
    nballs = 13
    dim    = 2
    x, y = skd.make_blobs(n_samples=n_samples,n_features=2,centers=nballs,cluster_std=0.2,
                        center_box=(-2.5,2.5),shuffle=False, random_state=20)
    n_pos = n_samples // 2
    y = y[:,np.newaxis]
    y[:n_pos] = 0
    y[n_pos:] = 1
    # print(y)
    # print(y.shape)
    # print(x.shape)


    x_torch = Variable(torch.from_numpy(x.astype(np.float32)).type(dtype))
    y_torch = Variable(torch.from_numpy(y.astype(np.float32)).type(dtype))


    hidden_layout = [100,30,10]
    inp_shape   = 2
    out_shape  = 1

    model = Linear1DModel(hidden_layout=hidden_layout,
                            inp_shape=inp_shape,
                            out_shape=out_shape,
                            cuda=True)
    npoints = 500
    # npoints = 10
    minimum = -3
    maximum = 3
    x_np=np.linspace(minimum, maximum, npoints)
    y_np=np.linspace(minimum, maximum, npoints)
    x_torchlattice = torch.from_numpy(x_np)
    y_torchlattice = torch.from_numpy(y_np)

    xx = x_torchlattice.view(-1, 1).repeat(1, len(x_np))
    yy = y_torchlattice.repeat(len(y_np),1)

    meshed = torch.cat([xx.unsqueeze_(2),yy.unsqueeze_(2)], 2)
    # print(meshed.size())
    y_out = model(meshed.type(dtype))
    # print(y_out.shape)


    y_plot = y_out.detach().cpu().numpy().squeeze()
    xx_plot= xx.detach().cpu().numpy().squeeze()
    yy_plot= yy.detach().cpu().numpy().squeeze()
    # print(y_plot.shape)
    # print(xx_plot.shape)
    # print(yy_plot.shape)
    fig = plt.figure(1)

    ax_2  = fig.add_subplot(111)

    print("y_plot.shape:",y_plot.shape)
    im = ax_2.imshow(y_plot,extent=[minimum,maximum,minimum,maximum],
                            vmin=0,vmax=1,
                            origin="lower")
    cbar = plt.colorbar(im,ticks=[0.,1.])
    ax_2.scatter(x[0:n_pos,0],x[0:n_pos,1],marker='o')
    ax_2.scatter(x[n_pos:,0] ,x[n_pos:,1],marker='+')
    # cbar.ax.set_xticklabels([0.,1.])



    #print(list(model.parameters()))
    #exit(1)
    # lr = 1e-1

    start = time.time()

    option_optimizers = {
        "SGD": torch.optim.SGD(model.parameters(), lr=1e-1,momentum=0.99),
        "Adagrad": torch.optim.Adagrad(model.parameters(), lr=1e-2),
        "Adam": torch.optim.Adam(model.parameters(), lr=1e-3,amsgrad=True),
        "NGD": NGD(model.parameters(), lr=1e-1, momentum=0.99, nesterov=True)
    }

    save_path = Path("save_fig/") / model_name
    save_path.mkdir(parents=True, exist_ok=True)

    optimizer = option_optimizers[model_name]


    n_epochs = 300
    for i in range(n_epochs):
        y_pred = model(x_torch)
        cost = (y_pred-y_torch).pow(2).mean()
        optimizer.zero_grad()
        cost.backward()
        # if i!=0:
        #     lr/=1.0003
        # change_lr(optimizer,lr)
        optimizer.step()
        y_out = model(meshed.type(dtype))
        y_plot = y_out.detach().cpu().numpy().squeeze()
        im.set_data(y_plot.T)
        # CS = QuadContourSet(ax_2, xx_plot, yy_plot, y_plot)
        # CS = ContourSet(ax_2, xx_plot, yy_plot, y_plot)
        # contour = ax_2.pcolorfast(xx_plot,yy_plot,y_plot,vmin=0,vmax=1)
        # plt.clabel(CS, inline=1, fontsize=10)

        plt.draw()
        plt.pause(0.005)
        print("i=",i," cost=",cost)
        if cost<1e-5:
            break
        fig.savefig(save_path / (str(i).zfill(5)+".png"))

    plt.close(fig)
    end = time.time()
    print('time elapsed:',end-start)
    # print("x_torch=",x_torch)
    # print("y_predicted=",y_predicted)
    # print("y_predicted.shape=",y_predicted.shape)
    # print("model.linear_1.weight=",model.linear_1.weight)
    # print("model.linear_1.bias=",model.linear_1.bias)


if __name__ == "__main__":
    typer.run(main)