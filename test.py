import torch
from torch.autograd import Variable
import numpy as np
import matplotlib.pyplot as plt
import time

dtype = torch.cuda.FloatTensor
#dtype = torch.FloatTensor
torch.set_default_tensor_type(dtype)
cudnn_version = torch.backends.cudnn.version()
print("cudnn version:",cudnn_version)
a = 4.
b = 3.
N = int(2e5)
t = np.linspace(0,4,N)
y = a*t+b
y_noise = y + np.random.normal(size=N)

a_tensor = Variable(torch.tensor([1.]),requires_grad=True).type(dtype)
b_tensor = Variable(torch.tensor([1.]),requires_grad=True).type(dtype)

y_noise_torch = Variable(torch.from_numpy(y_noise.astype(np.float32))).type(dtype)
t_torch       = Variable(torch.from_numpy(t.astype(np.float32)).type(dtype))


lrate = 1e-1
start = time.time()
for i in range(100):
    y_pred = a_tensor*t_torch+b_tensor
    cost = (y_pred-y_noise_torch).pow(2).mean()
    cost.backward()
    if i%25==0 and i!=0:
        lrate/=1.5
    print("i=",i," cost=",cost," lrate=",lrate)
    a_tensor.data -= lrate*a_tensor.grad.data
    b_tensor.data -= lrate*b_tensor.grad.data
    a_tensor.grad.data.zero_()
    b_tensor.grad.data.zero_()
end = time.time()
print('time elapsed:',end-start)
fig = plt.figure(1)
ax  = fig.add_subplot(111)
#ax.plot(t,y_noise,t,y)
#y_predicted = a_tensor.cpu().detach().numpy()*t+b_tensor.cpu().detach().numpy()
#ax.plot(t,y_predicted,label='prediction')
#ax.legend()
print(y_noise)
#plt.show()
