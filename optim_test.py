import torch
from torch.autograd import Variable
import numpy as np
import matplotlib.pyplot as plt
import time

dtype = torch.cuda.FloatTensor
#dtype = torch.FloatTensor
torch.set_default_tensor_type(dtype)
cudnn_version = torch.backends.cudnn.version()
print("cudnn version:",cudnn_version)
a = 4.
b = 3.
a_0 = 1.
b_0 = 1.
N = int(2e6)
#N = int(30)
t = np.linspace(0,4,N)
t = np.expand_dims(t,axis=1)
y = a*t+b
y_noise = y + np.random.normal(size=(N,1))


y_noise_torch = Variable(torch.from_numpy(y_noise.astype(np.float32))).type(dtype)
t_torch       = Variable(torch.from_numpy(t.astype(np.float32)).type(dtype))

class Linear1DModel(torch.nn.Module):
    def __init__(self,a_0,b_0,cuda=True):      
        super(Linear1DModel,self).__init__()
        if cuda:
            dtype = torch.cuda.FloatTensor
        else:
            dtype = torch.FloatTensor
        #self.a_tensor = Variable(torch.tensor([a_0]),requires_grad=True).type(dtype)
        #self.b_tensor = Variable(torch.tensor([b_0]),requires_grad=True).type(dtype)
        #self.a_tensor = torch.tensor([a_0],requires_grad=True).type(dtype)  
        #self.b_tensor = torch.tensor([b_0],requires_grad=True).type(dtype)
        self.linear = torch.nn.Linear(1,1)
        #torch.nn.init.constant_(self.linear,1.)
        #print(dir(self.linear))
        #exit(1)
    def forward(self,x):
        y_pred = self.linear(x)
        return y_pred

def change_lr(optim,lr):
    for g in optim.param_groups:
            g['lr'] = lr


model = Linear1DModel(a_0,b_0,cuda=True)
#print(list(model.parameters()))
#exit(1)
lr = 1e-1
start = time.time()
optimizer = torch.optim.SGD(model.parameters(), lr=lr)
for i in range(500):
    y_pred = model(t_torch)
    cost = (y_pred-y_noise_torch).pow(2).mean()
    optimizer.zero_grad()
    cost.backward()
    if i%25==0 and i!=0:
        lr/=1.15
    change_lr(optimizer,lr)
    optimizer.step()
    print("i=",i," cost=",cost," lr=",lr)
    
end = time.time()
print('time elapsed:',end-start)
fig = plt.figure(1)
ax  = fig.add_subplot(111)
y_predicted = model(t_torch).cpu().detach().numpy() 
print("t.shape=",t.shape)
print("y.shape=",y.shape)
print("y_noise.shape=",y_noise.shape)
print("y_predicted.shape=",y_predicted.shape)
print("model.linear.weight=",model.linear.weight)
print("model.linear.bias=",model.linear.bias)
plot = False
if plot:
    ax.plot(np.squeeze(t),np.squeeze(y_noise),np.squeeze(t),np.squeeze(y))
    ax.plot(t,y_predicted,label='prediction')
    ax.legend()
    plt.show()
